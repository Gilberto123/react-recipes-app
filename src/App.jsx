import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from './components/home/Home';
import RecipeDetails from './components/recipes/RecipeDetails';
import SearchResults from './components/search/SearchResults';

function App() {
  return (
    <Router>

      <Switch>

      <Route path='/search/:mealName'>
          <SearchResults />
        </Route>

        <Route path='/meal/:id'>
          <RecipeDetails />
        </Route>

        <Route path='/'>
          <Home />
        </Route>

      </Switch>

    </Router>
  );
}

export default App;
