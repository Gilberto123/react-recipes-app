import React, { useRef } from 'react'
import { useHistory } from 'react-router-dom'

const SearchForm = () => {

    const history = useHistory();

    const inputRef = useRef('')

    const handleSubmit = e => {
        e.preventDefault();
        const input = inputRef.current;
        const val = input.value;
        const redirectString = `/search/${val}`;
        history.push(redirectString);
    }


    return (
        <>
            <form onSubmit={handleSubmit} className="form-inline my-2 my-lg-0 ml-auto">
                <input ref={inputRef} className="form-control mr-sm-2" type="search" placeholder="Search" required />
                <button className="btn btn-success my-2 my-sm-0" type="submit">
                    <i className="fas fa-search"></i>
                </button>
            </form>
        </>
    )
}

export default SearchForm
