import React from 'react'
import { NavLink } from 'react-router-dom'
import SearchForm from './SearchForm'

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <span className="navbar-brand">React Recipes App</span>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-3">
                    <NavLink className='nav-item' to="/" activeClassName="active">
                        <span className='nav-link'>
                            Home
                        </span>
                    </NavLink>
                </ul>
                <SearchForm />
            </div>
        </nav>

    )
}

export default Navbar
