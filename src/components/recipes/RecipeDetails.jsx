import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Navbar from '../generic/Navbar';

/* utils */
import { API_URL } from '../../utils/constants'

const RecipeDetails = () => {

    const { id } = useParams();

    const initialState = {
        status: 'loading',
        meal: {}
    }

    const [state, setState] = useState(initialState)

    const getMeal = async () => {
        const response = await fetch(`${API_URL}/lookup.php?i=${id}`)
        const data = await response.json();
        const meal = data.meals[0];
        console.log(meal)
        setState({
            ...state,
            status: 'ready',
            meal
        })
    }

    useEffect(() => {
        getMeal();
    }, [])
    return (
        <>
            <Navbar />
            <div className="container-fluid">
                <div className="row mt-5">
                    <div className="col-md-12">
                        {
                            state.status === 'loading'
                                ? (
                                    <h2>Loading, please wait...</h2>
                                )
                                : (
                                    <>
                                        <div className="card">
                                            <img className="meal-img card-img-top" src={`${state.meal.strMealThumb}`} alt={state.meal.strMeal} />
                                            <div className="card-body">
                                                <h5 className="card-title">{state.meal.strMeal}</h5>
                                                <span className="badge badge-pill badge-primary mr-4 mb-3">{state.meal.strArea}</span>
                                                <span className="badge badge-pill badge-secondary mr-4 mb-3">{state.meal.strCategory}</span>
                                                {
                                                    state.meal.strYoutube !== ''
                                                    && (
                                                        <a
                                                            href={state.meal.strYoutube}
                                                            target='_blank'
                                                            rel="noreferrer"
                                                            className='btn btn-danger mb-2'
                                                        >
                                                            <i className="fab fa-youtube mr-3"></i>
                                                            View on YouTube
                                                        </a>
                                                    )
                                                }
                                                <p className="card-text mt-2">{state.meal.strInstructions}</p>
                                            </div>
                                        </div>

                                        {/* <h2>{state.meal.strMeal}</h2>
                                        <img src={`${state.meal.strMealThumb}`} alt={state.meal.strMeal} className="meal-img img-fluid"></img> */}
                                    </>
                                )
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default RecipeDetails
