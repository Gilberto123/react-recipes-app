import React, { useState, useEffect, useRef } from 'react'

/* utils */
import { API_URL } from '../../utils/constants'

const RecipesCategories = ({ changeCategory }) => {

    const initialState = {
        categories: [],
        status: 'loading'
    }

    const [state, setState] = useState(initialState)

    const categoriesContainerRef = useRef()

    const scroll = (evt) => {
        evt.preventDefault();
        categoriesContainerRef.current.scrollLeft += (evt.deltaY * 0.5);
    }

    const init = async () => {
        const response = await fetch(`${API_URL}/categories.php`);
        const data = await response.json();
        setState({
            ...state,
            categories: data.categories,
            status: 'ready'
        });

        categoriesContainerRef.current.addEventListener('wheel', scroll);
    }

    useEffect(() => {
        init();
    }, [])

    return (
        <div className='container-fluid'>
            {
                state.status === 'loading'
                    ? (
                        <h2>Loading, please wait...</h2>
                    )
                    : (
                        <div ref={categoriesContainerRef} className='cat-container'>
                            {
                                state.categories.map(c => (
                                    <div onClick={ () => changeCategory(c.strCategory)} key={c.idCategory} className='cat text-center'>
                                        <img src={`${c.strCategoryThumb}`} alt={`${c.strCategory}`} className="cat-recipes-img rounded-circle"></img>
                                        <br />
                                        <strong>{c.strCategory}</strong>
                                    </div>
                                ))
                            }
                        </div>
                    )
            }
        </div>
    )
}

export default RecipesCategories
