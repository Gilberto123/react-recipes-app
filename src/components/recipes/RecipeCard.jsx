import React from 'react'
import { Link } from 'react-router-dom'

const RecipeCard = ({ meal }) => {
    return (
        <div className="card">
            <img className="card-img-top" src={`${meal.strMealThumb}`} alt={meal.strMeal} />
            <div className="card-body">
                <p className="card-text">
                    <strong>
                        {meal.strMeal}
                    </strong>
                </p>
                <Link
                    to={`/meal/${meal.idMeal}`}
                    className="btn btn-primary"
                >
                    View Details
                </ Link>
            </div>
        </div>
    )
}

export default RecipeCard
