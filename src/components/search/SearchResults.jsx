import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Navbar from '../generic/Navbar'
import { API_URL } from '../../utils/constants'
import RecipeCard from '../recipes/RecipeCard'

const SearchResults = () => {
    const { mealName } = useParams();

    const initialState = {
        status: 'loading',
        meals: null
    }

    const [state, setState] = useState(initialState)

    const searchMeal = async () => {
        const response = await fetch(`${API_URL}/search.php?s=${mealName}`)
        const result = await response.json();
        const meals = result.meals
        console.log(meals)
        setState({
            ...state,
            status: 'ready',
            meals
        })
    }

    useEffect(() => {
        searchMeal();
    }, [mealName])

    return (
        <>
            <Navbar />
            <div className="container-fluid">
                <div className="row justify-content-around my-5">
                    {
                        state.status === 'loading' && (<h2>Loading, please wait...</h2>)
                    }
                    {
                        state.meals
                        ? (
                            state.meals.map(m => (
                                <div key={m.idMeal} className="col-md-2">
                                    <RecipeCard
                                        meal={m}
                                    />
                                </div>
                            ))
                        )
                        : (
                            <h2>No meals found</h2>
                        )
                    }

                </div>
            </div>
        </>
    )
}

export default SearchResults
