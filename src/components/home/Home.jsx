import React, { useEffect, useState } from 'react'

/* utils */
import { API_URL } from '../../utils/constants'

/* Components */
import Navbar from '../generic/Navbar'
import RecipeCard from '../recipes/RecipeCard'
import RecipesCategories from '../recipes/RecipesCategories'

const Home = () => {

    const initialState = {
        status: 'loading',
        category: 'Beef',
        meals: []
    }

    const [state, setState] = useState(initialState)

    const getByCategory = async () => {
        const cat = state.category
        const response = await fetch(`${API_URL}/filter.php?c=${state.category}`);
        console.log(`${API_URL}/filter.php?c=${state.category}`)
        const data = await response.json();
        const meals = data.meals;
        setState({
            ...state,
            status: 'ready',
            category: cat,
            meals
        })
    }

    const changeCategory = (cat) => {
        setState({
            ...state,
            category: cat,
        })
    }

    useEffect(() => {
        getByCategory();
    }, [state.category])

    return (
        <>
            {
                state.status === 'loading'
                    ? (
                        <h2>Loading, please wait...</h2>
                    )
                    : (
                        <>
                            <Navbar />
                            <RecipesCategories
                                changeCategory={changeCategory}
                            />
                            <div className='container-fluid'>
                                <div className="row mt-4 p-2">
                                    {
                                        state.meals == null && (
                                            <div className="alert alert-danger container" role="alert">
                                                No meals available!
                                            </div>

                                        )
                                    }
                                    {
                                        state.meals && state.meals.map(m => (
                                            <div key={m.idMeal} className="col-md-2 my-3">
                                                <RecipeCard
                                                    meal={m}
                                                />
                                            </div>

                                        ))
                                    }
                                </div>
                            </div>
                        </>
                    )
            }
        </>
    )
}

export default Home
